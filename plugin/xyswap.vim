" xyswap.vim - Quickly swap x and y values, widths and heights.
" Maintainer:  Bruce Hill <https://bruce-hill.com>
" Version: 0.0.1

if exists("g:loaded_xyswap")
    finish
endif
let g:loaded_xyswap = 1

command! -range XYSwap call xy#swap(<line1>, <line2>)
vnoremap <silent> <Plug>XYSwap :XYSwap<CR>
nnoremap <silent> <Plug>XYSwap :XYSwap<CR>
vnoremap <silent> <Plug>XYCopySwap :copy '><CR>`[V`]:XYSwap<CR>
nnoremap <silent> <Plug>XYCopySwap yyp:XYSwap<CR>

if !hasmapto('<Plug>XYSwap', 'n') && mapcheck('<leader>x', 'n') == ""
    nmap <silent> <leader>x <Plug>XYSwap
endif
if !hasmapto('<Plug>XYSwap', 'v') && mapcheck('<leader>x', 'v') == ""
    vmap <silent> <leader>x <Plug>XYSwap
endif
if !hasmapto('<Plug>XYCopySwap', 'n') && mapcheck('<leader>X', 'n') == ""
    nmap <silent> <leader>X <Plug>XYCopySwap
endif
if !hasmapto('<Plug>XYCopySwap', 'v') && mapcheck('<leader>X', 'v') == ""
    vmap <silent> <leader>X <Plug>XYCopySwap
endif
