" XYSwap plugin for Vim (swaps x and y)

if !exists("g:loaded_xyswap")
    finish
endif

function! s:apply_case(template, target)
    let result = a:target
    if a:template[0] ==# toupper(a:template[0])
        let result = toupper(result[0]) .. result[1:]
    else
        let result = tolower(result[0]) .. result[1:]
    endif
    if a:template[1:] ==# toupper(a:template[1:])
        let result = result[0] .. toupper(result[1:])
    else
        let result = result[0] .. tolower(result[1:])
    endif
    return result
endfunction

function! xy#swap(line_start, line_end)
    let wordpatt = '\C[^a-zA-Z][a-zA-Z]\|[a-z][A-Z]'
    let pattsubs = [['width', 'height'], ['column', 'row'], ['col', 'row'], ['x', 'y'], ['w', 'h'], ['dx', 'dy']]
    let L = a:line_start
    while L <= a:line_end
        let line = getline(L)
        let i = 0
        let modifications = 0
        while 1
            "echo "Checking: "..line[i:-1]
            for pair in pattsubs
                if line[i:i+strlen(pair[0])-1] ==? pair[0]
                    "echo "Matched "..pair[0]
                    let line = line[0:i-1] .. s:apply_case(line[i:i+strlen(pair[0])-1], pair[1]) .. line[i+strlen(pair[0]):]
                    let modifications += 1
                    let i += strlen(pair[1])
                    break
                elseif line[i:i+strlen(pair[1])-1] ==? pair[1]
                    "echo "Matched "..pair[1]
                    let line = line[0:i-1] .. s:apply_case(line[i:i+strlen(pair[1])-1], pair[0]) .. line[i+strlen(pair[1]):]
                    let modifications += 1
                    let i += strlen(pair[0])
                    break
                endif
            endfor
            let m = match(line, wordpatt, i)
            "echo 'match("'..line..'", "'..wordpatt..'", '..i..') = '..m
            if m == -1
                break
            endif
            let i = m + 1
        endwhile
        if modifications > 0
            silent call setline(L, line)
        endif
        let L += 1
    endwhile
endfunction
