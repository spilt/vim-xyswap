# Vim-xyswap

Vim-xyswap is a simple plugin to reduce the tedium of writing code that deals
with both x and y coordinates, rows and columns, widths and heights, etc. 

Default bindings are:

- `<leader>x`: swap the x/y of selection (or line)
- `<leader>X`: duplicate selection (or line) with x/y swapped

For example, if your cursor is on the line:

    x = min(0, max(SCREEN_WIDTH, x + dt * velX));

and you press `<leader>x`, the line will become:

    y = min(0, max(SCREEN_HEIGHT, y + dt * velY));

If you instead press `<leader>X`, the modified line will be added below the
current line instead of replacing it.
